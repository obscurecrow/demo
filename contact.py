
class Contact:
    def __init__(self, name, home=None, work=None, portables=[], emails=[]):
        self.name = name
        self.home = home
        self.work = work
        self.portables = portables
        self.emails = emails

    def get_name(self):
        return self.name

    def get_home(self):
        return self.home

    def get_work(self):
        return self.work

    def get_portables(self):
        return self.portables

    def get_emails(self):
        return self.emails
