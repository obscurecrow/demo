from constants import JsonKeys as JK
import json


class Access:

    def __init__(self, filename):
        self.filename = filename
        with open(self.filename) as contacts_file:
            self.contact_list = json.load(contacts_file).get(JK.CONTACTS, [])

    def get_names(self):
        return [contact.get(JK.NAME, "Name Unknown") for contact in self.contact_list]

    def get_contacts_by_name(self, name):
        return [contact for contact in self.contact_list if contact.get(JK.NAME) == name]

    def write_contact(self, contact):
        if not self.check_presence(contact.get_name()):
            self.contact_list.append(self._encode_contact(contact))
            return True
        else:
            return False

    def delete_contact(self, name):
        if self.check_presence(name):
            self.contact_list = [contact for contact in self.contact_list if contact.get(JK.NAME) != name]
            return True
        return False

    @staticmethod
    def _encode_contact(contact):
        dict_contact = {JK.NAME: contact.get_name()}
        if contact.get_home():
            dict_contact[JK.HOME] = contact.get_home()
        if contact.get_work():
            dict_contact[JK.WORK] = contact.get_work()
        if contact.get_portables():
            dict_contact[JK.PORTABLES] = contact.get_portables()
        if contact.get_emails():
            dict_contact[JK.EMAILS] = contact.get_emails()
        return dict_contact

    def close(self):
        with open(self.filename, "w") as outfile:

            json.dump({JK.CONTACTS: self.contact_list}, outfile)

    def check_presence(self, contact_name):
        if contact_name in [contact.get(JK.NAME) for contact in self.contact_list]:
            # The contact name is already in the contact list.
            return True
        return False
