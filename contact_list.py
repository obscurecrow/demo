from argparse import ArgumentParser
from interface import Interface

filename = None
parser = ArgumentParser()
parser.add_argument("-f", "--filename", dest=filename, help="Use FILENAME as contact list instead of the default")
args = parser.parse_args()
interface = Interface(args.filename)

while True:
    command = input("Please, enter a command. "
                    "C to create a new contact, "
                    "R to see all contact names, "
                    "L to look for a contact by name, "
                    "and D to delete a contact. "
                    "Hit Enter to exit. ").upper()
    if not command:
        interface.close_session()
        break
    if command == "C":
        interface.add_contact()
    elif command == "R":
        interface.print_names()
    elif command == "L":
        interface.search_contact()
    elif command == "D":
        interface.delete_contact()
    else:
        print("Command not recognized, try again.")
