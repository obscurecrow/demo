from access import Access
import constants as C
from constants import JsonKeys as JK
from contact import Contact
import sys


class Interface:

    def __init__(self, filename=None):
        if not filename:
            filename = C.CONTACT_LIST
        try:
            self.access = Access(filename)
        except IOError as e:
            print("Error encountered: {}. Please retry.".format(e))
            sys.exit()

    def print_names(self):
        print("Contacts:")
        for name in self.access.get_names():
            print(name)

    def search_contact(self):
        name = input("Write the name you would like to look for: ")
        if not name:
            print("Search aborted")
            return
        contacts = self.access.get_contacts_by_name(name)
        for contact in contacts:
            self.print_contact(contact)

    def add_contact(self):
        name = None
        portables = []
        emails = []
        while not name:
            name = input("Please insert the name of the new contact: ")
        home = input("Please insert the home contact (Enter for none): ")
        work = input("Please insert the work contact (Enter for none): ")
        while True:
            portable = input("Please insert the portable contact (Enter for none): ")
            if not portable:
                break
            portables.append(portable)
        while True:
            email = input("Please insert the contact's email (Enter for none): ")
            if not email:
                break
            emails.append(email)
        contact = Contact(name, home, work, portables, emails)
        written = self.access.write_contact(contact)
        if not written:
            print("The name you are trying to add is already in the contacts. You should update it instead.")

    def delete_contact(self):
        name = input("Please, insert the name of the contact you wish to delete: ")
        deleted = self.access.delete_contact(name)
        if deleted:
            print("Contact deleted.")
        else:
            print("No contact by that name.")

    @staticmethod
    def print_contact(contact):
        print(contact.get(JK.NAME, "Name Unknown"))
        if contact.get(JK.HOME):
            print("Home number: {}".format(contact[JK.HOME]))
        if contact.get(JK.WORK):
            print("Office number: {}".format(contact[JK.WORK]))
        if contact.get(JK.PORTABLES):
            for portable in contact[JK.PORTABLES]:
                print("Portable: {}".format(portable))
        if contact.get(JK.EMAILS):
            for email in contact[JK.EMAILS]:
                print("Email: {}".format(email))

    def close_session(self):
        self.access.close()
