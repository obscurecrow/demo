CONTACT_LIST = "./contacts/contact_list.json"


class JsonKeys:
    CONTACTS = "contacts"
    NAME = "name"
    HOME = "home"
    WORK = "work"
    PORTABLES = "portables"
    EMAILS = "emails"
